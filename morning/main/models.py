from django.db import models

# Create your models here.
from rest_framework import serializers


class Route(models.Model):
    title = models.CharField(max_length=150)
    priority = models.IntegerField(default=10)


class Step(models.Model):
    title = models.CharField(max_length=150)
    priority = models.IntegerField(default=10)
    route = models.ForeignKey(Route)
    details = models.TextField(blank=True)


class RouteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Route
        fields = ('title', 'priority')


class StepSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Step
        fields = ('title', 'priority', 'route', 'details')
