from django.contrib import admin
from .models import Route, Step

# Register your models here.
@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'priority', )
    list_editable = ('title', 'priority', )

    class Meta:
        ordering = ('-priority', 'title')


@admin.register(Step)
class StepAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'priority',)
    list_editable = ('title', 'priority',)
    list_filter = ('route',)

    class Meta:
        ordering = ('-priority', 'title')
